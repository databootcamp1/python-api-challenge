# python-api-challenge
## Creator: Melissa Acevedo

## Part 1: WeatherPy

The OpenWeatherMap API was used to retrieve weather data from the cities list generated in this code. Next, a series of scatter plots were prepared to showcase the following relationships:

- Latitude vs. Temperature

- Latitude vs. Humidity

- Latitude vs. Cloudiness

- Latitude vs. Wind Speed

Separate scatter plots were also created (Northern Hemisphere and Southern Hemisphere) in order to show the linear regression plots. See the details below:

- Northern Hemisphere: Temperature vs. Latitude

- Southern Hemisphere: Temperature vs. Latitude

- Northern Hemisphere: Humidity vs. Latitude

- Southern Hemisphere: Humidity vs. Latitude

- Northern Hemisphere: Cloudiness vs. Latitude

- Southern Hemisphere: Cloudiness vs. Latitude

- Northern Hemisphere: Wind Speed vs. Latitude

- Southern Hemisphere: Wind Speed vs. Latitude

## Part 2: VacationPy

Created a map that displays a point for every city in the city_data_df DataFrame. I considered these weather conditions:

- A max temperature lower than 27 degrees but higher than 21

- Wind speed less than 4.5 m/s

- Zero cloudiness

Created a new DataFrame called hotel_df to store the city, country, coordinates, and humidity.

For each city, I used the Geoapify API to find the first hotel located within 10,000 meters of my coordinates.

The hotel name and the country were added as additional information in the hover message for each city on the map.
